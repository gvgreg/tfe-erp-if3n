<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::group(['prefix' => 'auth'], function ($router) {

    Route::post('login', 'AuthController@login');
    Route::post('logout', 'AuthController@logout');
    Route::post('refresh', 'AuthController@refresh');
    Route::post('me', 'AuthController@me');

});

Route::group(['middelware' => 'jwt.auth'], function ($router) {

    Route::put('profil/edit/{id}', 'API\UserController@updateProfile');

    Route::prefix('users')->group( function() {
        Route::get('', 'UsersController@all');
        Route::post('/create', 'UsersController@new');
        Route::get('/{id}', 'UsersController@show');
        Route::delete('/{id}', 'UsersController@destroy');
    });

    Route::prefix('formations')->group( function() {
        Route::get('', 'API\FormationController@index')->name('formations.index');
        Route::get('/all', 'API\FormationController@all')->name('formations.all');
        Route::get('/latest', 'API\FormationController@latest')->name('formations.latest');
        Route::post('/create', 'API\FormationController@store')->name('formations.store');
        Route::put('/edit/{id}', 'API\FormationController@update')->name('formations.update');
        Route::get('/show/{id}', 'API\FormationController@show')->name('formations.show');
        Route::delete('/{id}', 'API\FormationController@destroy')->name('formations.destroy');
        Route::post('/addInscrit/{formation}/{inscrit}', 'API\FormationController@addInscrit')->name('formations.addInscrit');
        Route::delete('/deleteInscrit/{formation}/{inscrit}', 'API\FormationController@deleteInscrit')->name('formations.deleteInscrit');
    });
    Route::get('search/formations/{colonne}/{search}', 'API\FormationController@search');

    Route::prefix('recrutements')->group( function() {
        Route::get('', 'API\RecrutementController@index')->name('recrutements.index');
        Route::get('/all', 'API\RecrutementController@all')->name('recrutements.all');
        Route::post('', 'API\RecrutementController@store')->name('recrutements.store');
        Route::put('/{id}', 'API\RecrutementController@update')->name('recrutements.update');
        Route::get('/show/{id}', 'API\RecrutementController@show')->name('recrutements.show');
        Route::delete('/{id}', 'API\RecrutementController@destroy')->name('recrutements.destroy');
        Route::post('/addInscrit/{recrutement}/{inscrit}', 'API\RecrutementController@addInscrit')->name('recrutements.addInscrit');
        Route::delete('/deleteInscrit/{recrutement}/{inscrit}', 'API\RecrutementController@deleteInscrit')->name('recrutements.deleteInscrit');
    });
    Route::get('search/recrutements/{colonne}/{search}', 'API\RecrutementController@search');

    Route::prefix('inscrits')->group( function() {
        Route::get('', 'API\InscritController@index')->name('inscrits.index');
        Route::get('/latest', 'API\InscritController@latest')->name('inscrits.latest');
        Route::post('/create', 'API\InscritController@store')->name('inscrits.store');
        Route::put('/statut/{id}', 'API\InscritController@modifStatut')->name('statut-inscrit.update');
        Route::put('/edit/{id}', 'API\InscritController@update')->name('inscrits.update');
        Route::get('/show/{id}', 'API\InscritController@show')->name('inscrits.show');
        Route::get('search', 'API\InscritSearchController@index');
        Route::delete('/{id}', 'API\InscritController@destroy')->name('inscrits.destroy');
    });
    Route::get('search/inscrits/{colonne}/{search}', 'API\InscritController@search');

    Route::prefix('villes')->group( function() {
        Route::get('', 'API\VilleController@index')->name('villes.index');
    });

    Route::prefix('tags')->group( function() {
        Route::get('', 'API\TagController@index')->name('tags.index');
        Route::get('/all', 'API\TagController@all')->name('tags.all');
        Route::post('/create', 'API\TagController@store')->name('tags.store');
        Route::put('/edit/{id}', 'API\TagController@update')->name('tags.update');
        Route::delete('/{id}', 'API\TagController@destroy')->name('tags.destroy');
    });
    Route::get('search/tags/{colonne}/{search}', 'API\TagController@search');

    Route::prefix('prospects')->group( function() {
        Route::get('', 'API\InscritController@prospects')->name('prospects.index');
    });
    Route::get('search/prospects/{colonne}/{search}', 'API\InscritController@searchProspects');

    Route::prefix('pouvsub-infos')->group( function() {
        Route::get('/infos/{id}', 'API\PouvsubInfosController@getInfos')->name('pouvsub-infos.infos');
        Route::post('/create', 'API\PouvsubInfosController@store')->name('pouvsub-infos.store');
        Route::put('/edit/{id}', 'API\PouvsubInfosController@update')->name('pouvsub-infos.update');
        Route::delete('/{id}', 'API\PouvsubInfosController@destroy')->name('pouvsub-infos.destroy');
    });

    Route::apiResource('pouvsubs', 'API\PouvsubController');
    Route::get('search/pouvsubs/{colonne}/{search}', 'API\PouvsubController@search');
});
