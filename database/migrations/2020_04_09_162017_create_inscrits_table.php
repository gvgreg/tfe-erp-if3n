<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInscritsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inscrits', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nom');
            $table->string('prenom');
            $table->enum('genre', ['homme', 'femme', '3e genre']);
            $table->date('date_naissance');
            $table->string('rue');
            $table->integer('numero');
            $table->integer('boite')->nullable();
            $table->bigInteger('ville_id')->unsigned();
            $table->string('email');
            $table->string('tel');
            $table->text('commentaire_inscrit')->nullable();
            $table->boolean('prospect');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inscrits');
    }
}
