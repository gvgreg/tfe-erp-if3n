<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model\InscritInfos;
use App\Model\PouvSubInfos;
use App\Model\Inscrit;
use Faker\Generator as Faker;

$factory->define(InscritInfos::class, function (Faker $faker) {
    $inscrit = Inscrit::all()->pluck('id')->toArray();
    $pouv_sub_infos = PouvSubInfos::all()->pluck('id')->toArray();
    $ages = $faker->numberBetween($min = 18, $max = 45);
    $age = [null, $ages];
    $statut_legaux = [null, 'célibataire', 'marié·e', 'cohabitant'];
    $diplomes = [null, 'CESS', 'CESI', 'Aucun', 'Graduat', 'Master'];
    $durees_chomage = $faker->numberBetween($min = 0, $max = 15);
    $duree_chomage = [null, $durees_chomage];
    $moyens_recrutement = [null, 'journal', 'facebook', 'forem', 'site internet'];
    $groupes_sociaux = [null, 'réfugié', 'sociale', 'médicale'];


    return [
        'pouvsubinfos_id' =>  $faker->randomElement($pouv_sub_infos),
        'inscrit_id' => $faker->unique()->randomElement($inscrit),
        'age' => $faker->randomElement($age),
        'num_national' => null,
        'statut_legal' => $faker->randomElement($statut_legaux),
        'diplome' => $faker->randomElement($diplomes),
        'duree_chomage' => $faker->randomElement($duree_chomage),
        'moyen_recrutement' => $faker->randomElement($moyens_recrutement),
        'groupe_social' => $faker->randomElement($groupes_sociaux)
    ];
});
