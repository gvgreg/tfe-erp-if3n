<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model\PouvSubInfos;
use App\Model\PouvSub;
use App\Model\Formation;
use Faker\Generator as Faker;

$factory->define(PouvSubInfos::class, function (Faker $faker) {
    $formation = Formation::all()->pluck('id')->toArray();
    $pouvsub = PouvSub::all()->pluck('id')->toArray();

    return [
        'formation_id'=> $faker->randomElement($formation),
        'pouvsub_id'=>$faker->randomElement($pouvsub),
        'adresse'=>true,
        'date_naissance'=>false,
        'age'=>false,
        'email'=>false,
        'num_national'=>true,
        'statut_legal'=>false,
        'diplome'=>false,
        'duree_chomage'=>false,
        'moyen_recrutement'=>false,
        'groupe_social'=>false,
    ];
});
