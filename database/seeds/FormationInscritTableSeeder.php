<?php

use Illuminate\Database\Seeder;
use App\Model\FormationInscrit;

class FormationInscritTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(FormationInscrit::class, 80)->create();

    }
}
