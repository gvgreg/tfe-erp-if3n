<?php

use Illuminate\Database\Seeder;
use App\User;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = [
            [
                'firstname'=>'Gregory',
                'lastname'=>'Van den Bergh',
                'email'=>'g.vandenbergh@interface3namur.be',
                'password'=> bcrypt('rootroot'),
                'fonction'=>'Développeur de l\'ERP',
                'role'=>'master',
                'avatar'=>'man.png',
            ],
            [
                'firstname'=>'Caroline',
                'lastname'=>'Marique',
                'email'=>'superadmin@test.be',
                'password'=> bcrypt('rootroot'),
                'fonction'=>'Co-responsable du parc informatique & Formatrice',
                'role'=>'superAdmin',
                'avatar'=>'woman.png',
            ],
            [
                'firstname'=>'Vincianne',
                'lastname'=>'Rouard',
                'email'=>'admin@test.be',
                'password'=> bcrypt('rootroot'),
                'fonction'=>'Coordinatrice adminsitrative',
                'role'=>'admin',
                'avatar'=>'woman.png',
            ],
            [
                'firstname'=>'Alain',
                'lastname'=>'Fustin',
                'email'=>'formateur1@test.be',
                'password'=> bcrypt('rootroot'),
                'fonction'=>'Formateur',
                'role'=>'formateur',
                'avatar'=>'man.png',
            ],
            [
                'firstname'=>'Aline',
                'lastname'=>'Renard',
                'email'=>'formateur2@test.be',
                'password'=> bcrypt('rootroot'),
                'fonction'=>'Chargée de Communication & Formatrice',
                'role'=>'formateur',
                'avatar'=>'woman.png',
            ],
            [
                'firstname'=>'Elodie',
                'lastname'=>'Bayet',
                'email'=>'formateur3@test.be',
                'password'=> bcrypt('rootroot'),
                'fonction'=>'Formatrice',
                'role'=>'formateur',
                'avatar'=>'woman.png',
            ],
            [
                'firstname'=>'Cécilia',
                'lastname'=>'Icard',
                'email'=>'formateur4@test.be',
                'password'=> bcrypt('rootroot'),
                'fonction'=>'Chargée de Communication & Formatrice',
                'role'=>'formateur',
                'avatar'=>'woman.png',
            ],
            [
                'firstname'=>'Master',
                'lastname'=>'Master',
                'email'=>'master@tfe.be',
                'password'=> bcrypt('master'),
                'fonction'=>'Compte pour TFE',
                'role'=>'master',
                'avatar'=>'man.png',
            ],
            [
                'firstname'=>'Formateur',
                'lastname'=>'Formateur',
                'email'=>'formateur@tfe.be',
                'password'=> bcrypt('formateur'),
                'fonction'=>'Compte pour TFE',
                'role'=>'formateur',
                'avatar'=>'man.png',
            ],
            [
                'firstname'=>'Formatrice',
                'lastname'=>'Formatrice',
                'email'=>'formatrice@tfe.be',
                'password'=> bcrypt('formatrice'),
                'fonction'=>'Compte pour TFE',
                'role'=>'formatrice',
                'avatar'=>'woman.png',
            ],
        ];

        foreach ($user as $key => $value) {
            User::create($value);
        }
    }
}
