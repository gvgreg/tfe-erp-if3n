<?php

use Illuminate\Database\Seeder;
use App\Model\InscritInfos;

class InscritInfoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(InscritInfos::class, 150)->create();
    }
}
