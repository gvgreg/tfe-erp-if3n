<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UserTableSeeder::class);
        $this->call(PouvSubTableSeeder::class);
        $this->call(FormationsTableSeeder::class);
        $this->call(PouvSubInfosTableSeeder::class);
        $this->call(InscritsTableSeeder::class);
        $this->call(RecrutementsTableSeeder::class);
        $this->call(RecrutementInscritTableSeeder::class);
        $this->call(InscritInfoTableSeeder::class);
        $this->call(FormationInscritTableSeeder::class);
    }
}
