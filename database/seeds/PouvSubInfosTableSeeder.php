<?php

use Illuminate\Database\Seeder;
use App\Model\PouvSubInfos;

class PouvSubInfosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(PouvSubInfos::class, 8)->create();
    }
}
