<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PouvsubResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'nom' => $this->nom,
            'logo' => $this->logo,
            'adresse' => $this->adresse,
            'date_naissance' => $this->date_naissance,
            'age' => $this->age,
            'email' => $this->email,
            'num_national' => $this->num_national,
            'statut_legal' => $this->statut_legal,
            'diplome' => $this->diplome,
            'duree_chomage' => $this->duree_chomage,
            'moyen_recrutement' => $this->moyen_recrutement,
            'groupe_social' => $this->groupe_social,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}
