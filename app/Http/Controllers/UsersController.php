<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Http\Requests\CreateUserRequest;

class UsersController extends Controller
{
    public function all()
    {
        $users = User::all();

        return response()->json([
            'users' => $users
        ], 200);
    }

    public function show($id)
    {
        $user = User::whereId($id)->first();

        return response()->json([
            'user' => $user
        ], 200);
    }

    public function new(CreateUserRequest $request)
    {
        if($request['avatar'] === 'Homme') {
            $avatar = 'man.png';
        } else if ($request['avatar'] === 'Femme') {
            $avatar = 'woman.png';
        } else {
            $avatar = 'user.png';
        }

        $user = User::create([
            'firstname' => $request['firstname'],
            'lastname' => $request['lastname'],
            'email' => $request['email'],
            'password' => bcrypt($request['password']),
            'fonction' => $request['fonction'],
            'role' => $request['role'],
            'avatar' => $avatar,
        ]);

        return response()->json([
            'user' => $user
        ], 200);
    }

    public function destroy($id)
    {
        $user = User::findOrFail($id);

        $user->delete();

        return response()->json([
            'message' => 'L\'utilisateur est supprimé'
        ]);
    }
}
