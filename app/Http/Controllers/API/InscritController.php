<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Resources\InscritCollection;
use App\Http\Resources\InscritResource;
use App\Model\Formation;
use App\Model\FormationInscrit;
use App\Model\Inscrit;
use App\Model\InscritInfos;
use App\Model\InscritTag;
use App\Model\InscritTags;
use App\Model\PouvsubInfos;
use App\Model\Recrutement;
use App\Model\RecrutementInscrit;
use App\Model\Tag;
use Illuminate\Http\Request;

class InscritController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        $inscrits = Inscrit::orderBy('id', 'DESC')->paginate(10);

        return InscritResource::collection($inscrits);
    }

    /**
     * @param $field
     * @param $search
     * @return InscritCollection
     */
    public function search($field, $search)
    {
        return new InscritCollection(Inscrit::where($field,'LIKE',"%$search%")->latest()->paginate(10));
    }

    /**
     * @return InscritResource
     */
    public function latest()
    {
        return new InscritResource(Inscrit::orderBy('id', 'DESC')->first());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return InscritResource
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'nom' => 'required',
            'prenom' => 'required',
            'genre' => 'required',
            'date_naissance' => 'required',
            'tel' => 'required',
        ]);

        $inscrit = new Inscrit();

        $inscrit->nom = $request->nom;
        $inscrit->prenom = $request->prenom;
        $inscrit->genre = $request->genre;
        $inscrit->date_naissance = $request->date_naissance;
        $inscrit->rue = $request->rue;
        $inscrit->numero = $request->numero;
        $inscrit->boite = $request->boite;
        $inscrit->ville_id = $request->ville_id;
        $inscrit->email = $request->email;
        $inscrit->tel = $request->tel;
        $inscrit->commentaire_inscrit = $request->commentaire_inscrit;
        $inscrit->prospect = $request->prospect;
        $inscrit->age = $request->age;
        $inscrit->num_national = $request->num_national;
        $inscrit->statut_legal = $request->statut_legal;
        $inscrit->diplome = $request->diplome;
        $inscrit->duree_chomage = $request->duree_chomage;
        $inscrit->moyen_recrutement = $request->moyen_recrutement;
        $inscrit->groupe_social = $request->groupe_social;
        $inscrit->save();

        $idstags =  $request->tags;

        if($idstags !== []) {
            $tags = Tag::find($idstags);
            $inscrit->tags()->attach($tags);
        }

        $date_ajout = date('Y-m-d');

        $idrecrutement =  $request->recrutement;
        if($idrecrutement !== null) {
            $recrutement = Recrutement::find($idrecrutement);
            $inscrit->recrutements()->attach($recrutement, ['date_ajout' => $date_ajout]);
        }

        $idformation =  $request->formation;
        if($idformation !== null) {
            $formation = Formation::find($idformation);
            $inscrit->formations()->attach($formation, ['date_ajout' => $date_ajout]);
        }

        return new InscritResource($inscrit);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $inscrit = new InscritResource(Inscrit::findOrFail($id));
        $listTagsInscrit = InscritTag::where('inscrit_id', $id)->get()->all();
        $formationInscrit = FormationInscrit::where('inscrit_id', $id)->get()->last();
        $formationsInscrit = FormationInscrit::where('inscrit_id', $id)->get()->all();
        $recrutementsInscrit = RecrutementInscrit::where('inscrit_id', $id)->get()->all();

        $tags = [];

        if($listTagsInscrit != null) {
            for($i=0; $i < count($listTagsInscrit) ; $i++) {
                $tag = Tag::where('id',$listTagsInscrit[$i]->tag_id)->get()->first();
                array_push($tags, $tag);
            }
        }

        if($formationInscrit != null) {
            $pouvsubInfos = PouvsubInfos::where('formation_id', $formationInscrit->formation_id)->get()->first();
        } else {
            $pouvsubInfos = null;
        }

        $formations = [];

        if($formationsInscrit != null) {
            for($i=0; $i < count($formationsInscrit) ; $i++) {
                $formation = Formation::where('id', $formationsInscrit[$i]->formation_id)->get()->first();
                array_push($formations, $formation);
            }
        }

        $recrutements = [];
        $recrutementsInfos = [];


        if($recrutementsInscrit != null) {
            for($i=0; $i < count($recrutementsInscrit) ; $i++) {
                $recrutement = Recrutement::where('id', $recrutementsInscrit[$i]->recrutement_id)->get()->first();
                array_push($recrutements, $recrutement);
            }

            for($i=0; $i < count($recrutements) ; $i++) {
                $formationInfos = Formation::where('id', $recrutements[$i]->formation_id)->get()->first();
                array_push($recrutementsInfos, $formationInfos);
            }
        }


        return response()->json([
            'inscrit' => $inscrit,
            'formationInscrit' => $formationInscrit,
            'recrutements' => $recrutements,
            'recrutementsInfos' => $recrutementsInfos,
            'pouvsubInfos' => $pouvsubInfos,
            'formations' => $formations,
            'tags' => $tags,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return InscritResource
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'nom' => 'required',
            'prenom' => 'required',
            'genre' => 'required',
            'date_naissance' => 'required',
            'tel' => 'required',
        ]);

        $inscrit = Inscrit::findOrFail($id);

        $inscrit->nom = $request->nom;
        $inscrit->prenom = $request->prenom;
        $inscrit->genre = $request->genre;
        $inscrit->date_naissance = $request->date_naissance;
        $inscrit->rue = $request->rue;
        $inscrit->numero = $request->numero;
        $inscrit->boite = $request->boite;
        $inscrit->ville_id = $request->ville_id;
        $inscrit->email = $request->email;
        $inscrit->tel = $request->tel;
        $inscrit->commentaire_inscrit = $request->commentaire_inscrit;
        $inscrit->prospect = $request->prospect;
        $inscrit->age = $request->age;
        $inscrit->num_national = $request->num_national;
        $inscrit->statut_legal = $request->statut_legal;
        $inscrit->diplome = $request->diplome;
        $inscrit->duree_chomage = $request->duree_chomage;
        $inscrit->moyen_recrutement = $request->moyen_recrutement;
        $inscrit->groupe_social = $request->groupe_social;
        $inscrit->save();

        return new InscritResource($inscrit);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return InscritResource
     */
    public function destroy($id)
    {
        $inscrit = Inscrit::find($id);

        $inscrit->delete();

        return new InscritResource($inscrit);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function prospects()
    {
        $prospects = Inscrit::where('prospect', 1)->orderBy('id', 'DESC')->paginate(10);

        return InscritResource::collection($prospects);
    }

    /**
     * @param $field
     * @param $search
     * @return InscritCollection
     */
    public function searchProspects($field, $search)
    {
        return new InscritCollection(Inscrit::where($field,'LIKE',"%$search%")->latest()->paginate(10));
    }

    public function modifStatut($id)
    {
        $inscrit = Inscrit::find($id);

        $inscrit->prospect = 1;
        $inscrit->save();

        return new InscritResource($inscrit);
    }

    public function showSearch($id)
    {
        $inscrit = new InscritResource(Inscrit::findOrFail($id));

        return response()->json($inscrit);
    }
}
