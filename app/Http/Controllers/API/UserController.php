<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    public function updateProfile(Request $request, $id)
    {
        $this->validate($request, [
            'firstname' => 'required|string|max:191',
            'lastname' => 'required|string|max:191',
            'email' => 'required|string|email|max:191|unique:users,email,'.$id,
            'password' => 'sometimes|required|min:8',
        ]);

        $user=User::findOrFail($id);

        $user->firstname = $request->firstname;
        $user->lastname = $request->lastname;
        $user->email = $request->email;
        if(!empty($request->password)){
            $request->merge(['password' => bcrypt($request['password'])]);
        }
        $user->password = $request->password;


//        $currentAvatar = $user->avatar;
//        if($request->avatar != $currentAvatar){
//            // Récupère l'extension du fichier
//            $name = time().'.' . explode('/', explode(':', substr($request->avatar, 0, strpos($request->avatar, ';')))[1])[1];
//            // Enregistre le fichier dans le dossier 'avatars'
//            \Image::make($request->avatar)->save(public_path('images/avatars/').$name);
//            // Push dans la BDD
//            $request->merge(['avatar' => $name]);
//            // Suppresion de l'ancien avatar si nouvel avatar uploadé
//            $userAvatar = public_path('images/avatars/').$currentAvatar;
//            if(file_exists($userAvatar)){
//                @unlink($userAvatar);
//            }
//        }



//        $user->update($request->all());
        $user->save();

        return response()->json([
            'message' => 'Profil édité'
        ]);
    }
}
