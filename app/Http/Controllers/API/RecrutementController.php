<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Resources\FormationResource;
use App\Http\Resources\RecrutementCollection;
use App\Http\Resources\RecrutementResource;
use App\Model\Formation;
use App\Model\Inscrit;
use App\Model\RecrutementInscrit;
use Illuminate\Http\Request;
use App\Model\Recrutement;


class RecrutementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        $recrutements = Recrutement::orderBy('id', 'DESC')->paginate(10);

        return RecrutementResource::collection($recrutements);
    }

    /**
     * @param $field
     * @param $search
     * @return RecrutementCollection
     */
    public function search($field, $search)
    {
        return new RecrutementCollection(Recrutement::where($field,'LIKE',"%$search%")->latest()->paginate(10));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function all()
    {
        $recrutements = Recrutement::all();
        $listRecrutements = RecrutementResource::collection($recrutements);
        $candidats = [];

        for($x=0; $x < count($recrutements); $x++) {
            $inscrits= 0;
            $inscritsRecrutement = RecrutementInscrit::where('recrutement_id', $recrutements[$x]->id)->get()->all();
            for($y=0; $y < count($inscritsRecrutement); $y++) {
                $inscrits++;
            }
            array_push($candidats, $inscrits);
        }

        return response()->json([
            'recrutements' => $listRecrutements,
            'candidats' => $candidats,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return RecrutementResource
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'formation_id' => 'required',
            'date' => 'required',
        ]);

        $recrutement = new Recrutement();

        $recrutement->formation_id = $request->formation_id;
        $recrutement->date = $request->date;
        $recrutement->save();

        return new RecrutementResource($recrutement);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $recrutement = new RecrutementResource(Recrutement::findOrFail($id));
        $listCandidats = RecrutementInscrit::where('recrutement_id', $id)->get()->all();
        $candidats = [];

        for($i=0; $i<count($listCandidats); $i++) {
            $inscrit = Inscrit::where('id', $listCandidats[$i]->inscrit_id)->get()->first();
            array_push($candidats, $inscrit);
        }

        return response()->json([
            'recrutement' => $recrutement,
            'candidats' => $candidats,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return RecrutementResource
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'formation_id' => 'required',
            'date' => 'required',
        ]);

        $recrutement = Recrutement::findOrFail($id);

        $recrutement->formation_id = $request->formation_id;
        $recrutement->date = $request->date;
        $recrutement->save();

        return new RecrutementResource($recrutement);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return RecrutementResource
     */
    public function destroy($id)
    {
        $recrutement = Recrutement::find($id);

        $recrutement->delete();

        return new RecrutementResource($recrutement);
    }

    public function addInscrit($id, $idInscrit)
    {
        $recrutement = Recrutement::find($id);
        $inscrit = Inscrit::find($idInscrit);
        $message = null;
        $error= null;
        $test = false;

        $date_ajout = date('Y-m-d');

        $recrutementsInscrit = RecrutementInscrit::where('inscrit_id', $idInscrit)->get()->all();

        for($i=0; $i < count($recrutementsInscrit); $i++) {
            if ($id == $recrutementsInscrit[$i]->recrutement_id) {
                $test = true;
            }
        }
        if($test == false) {
            $recrutement->inscrits()->attach($inscrit, ['date_ajout' => $date_ajout]);
            $message = 'Inscription effectuée';
        } else {
            $error = 'Déjà inscrit à cette formation';
        }

        return response()->json([
            'message' => $message,
            'error' => $error,
        ]);
    }

    public function deleteInscrit($id, $idInscrit)
    {
        $recrutementInscrit = RecrutementInscrit::where('inscrit_id', $idInscrit)->where('recrutement_id', $id)->get()->first();

        $recrutementInscrit->delete();

        return response()->json([
            'message' => 'Inscription supprimée',
        ]);
    }
}
