<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Resources\FormationCollection;
use App\Http\Resources\FormationResource;
use App\Http\Resources\PouvsubInfosResource;
use App\Model\Formation;
use App\Model\FormationInscrit;
use App\Model\Inscrit;
use App\Model\Pouvsub;
use App\Model\PouvsubInfos;
use App\Model\Recrutement;
use Illuminate\Http\Request;

class FormationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        $formations = Formation::orderBy('id', 'DESC')->paginate(10);

        return FormationResource::collection($formations);
    }

    /**
     * @param $field
     * @param $search
     * @return FormationCollection
     */
    public function search($field, $search)
    {
        return new FormationCollection(Formation::where($field,'LIKE',"%$search%")->latest()->paginate(10));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function all()
    {
        $formations = Formation::all();

        $listFormations = FormationResource::collection($formations);
        $stagiaires = [];
        $nbreStagiaires = [];

        for($x=0; $x < count($formations); $x++) {
            $tempStagiaire = [];
            $maxStagiaires = null;
            $temp = [];
            $etat = '';
            $inscrits= 0;
            $min = $formations[$x]->min_stagiaires;
            $max = $formations[$x]->max_stagiaires;
            $inscritsFormation = FormationInscrit::where('formation_id', $formations[$x]->id)->get()->all();
            $nbreStagiaire = FormationInscrit::where('formation_id', $formations[$x]->id)->count();
            array_push($tempStagiaire, $nbreStagiaire);
            $maxStagiaires = $formations[$x]->max_stagiaires;
            array_push($tempStagiaire, $maxStagiaires);
            array_push($nbreStagiaires, $tempStagiaire);
            for($y=0; $y < count($inscritsFormation); $y++) {
                $inscrits++;
            }
            array_push($temp, $inscrits);
            if($inscrits < $min) {
                $etat = 'Insuffisant';
            } else if ($inscrits >= $min && $inscrits < $max) {
                $etat = 'Incomplet';
            } else if ($inscrits === $max) {
                $etat = 'Complet';
            }
            array_push($temp, $etat);
            array_push($stagiaires, $temp);
        }

        return response()->json([
            'formations' => $listFormations,
            'stagiaires' => $stagiaires,
            'nbreStagiaires' => $nbreStagiaires,
        ]);
    }

    /**
     * @return FormationResource
     */
    public function latest()
    {
        return new FormationResource(Formation::orderBy('id', 'DESC')->first());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return FormationResource
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'nom' => 'required',
            'date_debut' => 'required',
            'date_fin' => 'required',
            'min_stagiaires' => 'required',
            'max_stagiaires' => 'required',
            'nbre_heures' => 'required',
            'salle' => 'required',
            'prix' => 'required',
            'statut' => 'required',
            'user_id' => 'required',
            'pouvsub_id' => 'required',
            'abreviation' => 'required',
        ]);

        $formation = new Formation();

        $formation->nom = $request->nom;
        $formation->session = $request->session;
        $formation->date_debut = $request->date_debut;
        $formation->date_fin = $request->date_fin;
        $formation->min_stagiaires = $request->min_stagiaires;
        $formation->max_stagiaires = $request->max_stagiaires;
        $formation->nbre_heures = $request->nbre_heures;
        $formation->salle = $request->salle;
        $formation->prix = $request->prix;
        $formation->commentaire_formation = $request->commentaire_formation;
        $formation->statut = $request->statut;
        $formation->user_id = $request->user_id;
        $formation->pouvsub_id = $request->pouvsub_id;
        $formation->abreviation = $request->abreviation;
        $formation->save();

        return new FormationResource($formation);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $formation = new FormationResource(Formation::findOrFail($id));
        $infosPouvSub = PouvsubInfos::where('formation_id', $id)->get()->first();
        $idPouvSub = $formation->pouvsub_id;
        $pouvsub = Pouvsub::where('id', $idPouvSub)->get()->first();
        $recrutements = Recrutement::where('formation_id', $id)->get()->all();
        $listStagiaires = FormationInscrit::where('formation_id', $id)->get()->all();
        $stagiaires = [];
        $nbreStagiaires = FormationInscrit::where('formation_id', $id)->count();

        for($i=0; $i<count($listStagiaires); $i++) {
            $inscrit = Inscrit::where('id', $listStagiaires[$i]->inscrit_id)->get()->first();
            array_push($stagiaires, $inscrit);
        }

        return response()->json([
            'formation' => $formation,
            'infospouvsub' => $infosPouvSub,
            'pouvsub' => $pouvsub,
            'recrutements' => $recrutements,
            'stagiaires' => $stagiaires,
            'nbreStagiaires' => $nbreStagiaires,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return FormationResource
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'nom' => 'required',
            'date_debut' => 'required',
            'date_fin' => 'required',
            'min_stagiaires' => 'required',
            'max_stagiaires' => 'required',
            'nbre_heures' => 'required',
            'salle' => 'required',
            'prix' => 'required',
            'statut' => 'required',
            'user_id' => 'required',
            'pouvsub_id' => 'required',
            'abreviation' => 'required',
        ]);

        $formation = Formation::findOrFail($id);

        $formation->nom = $request->nom;
        $formation->session = $request->session;
        $formation->date_debut = $request->date_debut;
        $formation->date_fin = $request->date_fin;
        $formation->min_stagiaires = $request->min_stagiaires;
        $formation->max_stagiaires = $request->max_stagiaires;
        $formation->nbre_heures = $request->nbre_heures;
        $formation->salle = $request->salle;
        $formation->prix = $request->prix;
        $formation->commentaire_formation = $request->commentaire_formation;
        $formation->statut = $request->statut;
        $formation->user_id = $request->user_id;
        $formation->pouvsub_id = $request->pouvsub_id;
        $formation->abreviation = $request->abreviation;
        $formation->save();

        return new FormationResource($formation);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return FormationResource
     */
    public function destroy($id)
    {
        $formation = Formation::find($id);

        $formation->delete();

        return new FormationResource($formation);
    }

    public function addInscrit($id, $idInscrit)
    {
        $formation = Formation::find($id);
        $inscrit = Inscrit::find($idInscrit);
        $message = null;
        $error= null;
        $test = false;

        $date_ajout = date('Y-m-d');

        $formationsInscrit = FormationInscrit::where('inscrit_id', $idInscrit)->get()->all();

        for($i=0; $i < count($formationsInscrit); $i++) {
            if ($id == $formationsInscrit[$i]->formation_id) {
                $test = true;
            }
        }
        if($test == false) {
            $formation->inscrits()->attach($inscrit, ['date_ajout' => $date_ajout]);
            $message = 'Inscription effectuée';
        } else {
            $error = 'Déjà inscrit à cette formation';
        }

        return response()->json([
            'message' => $message,
            'error' => $error,
        ]);
    }

    public function deleteInscrit($id, $idInscrit)
    {
        $formationInscrit = FormationInscrit::where('inscrit_id', $idInscrit)->where('formation_id', $id)->get()->first();

        $formationInscrit->delete();

        return response()->json([
            'message' => 'Inscription supprimée',
        ]);
    }
}
