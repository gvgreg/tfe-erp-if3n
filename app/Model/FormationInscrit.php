<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class FormationInscrit extends Model
{
    protected $table = 'inscrit_formation';

    protected $fillable = [
        'date_ajout'
    ];

    public $timestamps = false;
}
